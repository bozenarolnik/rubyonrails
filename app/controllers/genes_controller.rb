class GenesController < ApplicationController
	def index
		@genes = Gene.all
	end
	
	def new
	  @gene = Gene.new
	end
	
	def create
	  gene_params = params.require(:gene).permit(:filename, :source_file)
	
	  @gene = Gene.new(gene_params)
	  if @gene.save
	    redirect_to genes_path
	  else
	    render :new
	  end
	end
end
