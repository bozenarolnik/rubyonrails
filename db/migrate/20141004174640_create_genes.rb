class CreateGenes < ActiveRecord::Migration
  def change
    create_table :genes do |t|
      t.string :filename

      t.timestamps
    end
  end
end
